# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "math_utils/version"

Gem::Specification.new do |s|
  s.name        = "math-utils"
  s.version     = MathUtils::VERSION
  s.authors     = ["Takayuki YAMAGUCHI"]
  s.email       = ["d@ytak.info"]
  s.homepage    = ""
  s.license     = "GPL3"
  s.summary     = "Miscellaneous files for numerical investigation"
  s.description = "Miscellaneous files for numerical investigation"

  s.rubyforge_project = "math-utils"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  # specify any dependencies here; for example:
  s.add_development_dependency "rspec"
  s.add_development_dependency "yard"
  s.add_development_dependency "bundler"
  s.add_development_dependency "rcov"
  s.add_runtime_dependency "ruby-xz"
  s.add_runtime_dependency "ruby-mpfr"
  s.add_runtime_dependency "ruby-mpfi"
end
