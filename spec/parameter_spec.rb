require "spec_helper"

describe ParameterRegion do
  context "when initializing" do
    it "should return initial parameter as it is." do
      pregion = ParameterRegion.new([0.0, 0.0])
      ary = pregion.to_a
      ary.should == [[0.0, 0.0]]
    end

    it "should raise error for an invalid index." do
      lambda do
        ParameterRegion.new([0.0, 0.0], '0' => [2.0, 0.5])
      end.should raise_error
    end

    it "should raise error for an invalid parameter range." do
      lambda do
        ParameterRegion.new([0.0, 0.0], 0 => 2.0)
      end.should raise_error
    end

    it "should raise error for an invalid dimension of range." do
      lambda do
        ParameterRegion.new([0.0, 0.0], 2 => [0.0, 2.0])
      end.should raise_error
    end
  end

  context "when getting properties" do
    it "should return varying indexes." do
      pregion = ParameterRegion.new([0.0, 0.0, 0.0], 2 => [1.0, 1.0], 0 => [1.0, 0.5])
      pregion.index_varying.should == [0, 2]
    end

    it "should return dimension." do
      pregion = ParameterRegion.new([0.0, 0.0], 0 => [2.0, 0.5])
      pregion.dim.should == 2
    end

    it "should return range of an index." do
      pregion = ParameterRegion.new([5.0, 0.0], 0 => [2.0, 0.5])
      pregion.range(0).should == [2.0, 5.0]
    end

    it "should return empty range of an index." do
      pregion = ParameterRegion.new([5.0, 0.0], 0 => [2.0, 0.5])
      pregion.range(1).should == [0.0, 0.0]
    end
  end

  context "when getting each elements" do
    it "should return an array of points on a line." do
      pregion = ParameterRegion.new([0.0, 0.0], 0 => [2.0, 0.5])
      ary = pregion.to_a
      ary.should == [[0.0, 0.0], [0.5, 0.0], [1.0, 0.0], [1.5, 0.0], [2.0, 0.0]]
    end

    it "should return an array of points on a line with reverse order." do
      pregion = ParameterRegion.new([2.0, 0.0], 0 => [0.0, 0.5])
      ary = pregion.to_a
      ary.should == [[2.0, 0.0], [1.5, 0.0], [1.0, 0.0], [0.5, 0.0], [0.0, 0.0]]
    end

    it "should return an array of points on a box." do
      pregion = ParameterRegion.new([0.0, 0.0], 0 => [1.0, 0.5], 1 => [1.0, 0.5])
      ary = pregion.to_a
      ary.should == [[0.0, 0.0], [0.0, 0.5], [0.0, 1.0],
                     [0.5, 0.0], [0.5, 0.5], [0.5, 1.0],
                     [1.0, 0.0], [1.0, 0.5], [1.0, 1.0]]
    end

    it "should return an array of points on a box." do
      pregion = ParameterRegion.new([0.0, 0.0, 0.0], 0 => [1.0, 1.0], 1 => [1.0, 1.0], 2 => [1.0, 0.5])
      ary = pregion.to_a
      ary.should == [[0.0, 0.0, 0.0], [0.0, 0.0, 0.5], [0.0, 0.0, 1.0],
                     [0.0, 1.0, 0.0], [0.0, 1.0, 0.5], [0.0, 1.0, 1.0],
                     [1.0, 0.0, 0.0], [1.0, 0.0, 0.5], [1.0, 0.0, 1.0],
                     [1.0, 1.0, 0.0], [1.0, 1.0, 0.5], [1.0, 1.0, 1.0]]
    end
  end

  context "when fixing indexes" do
    it "should fix an index." do
      pregion = ParameterRegion.new([0.0, 0.0], 0 => [2.0, 0.5], 1 => [1.0, 0.5])
      ary = pregion.fix_index(0) do |ps|
        ps.index_varying.should == [1]
      end
    end

    it "should fix an index (2)." do
      pregion = ParameterRegion.new([0.0, 0.0], 1 => [1.0, 0.5])
      ary = pregion.fix_index(1) do |ps|
        ps.index_varying.should == []
      end
    end

    it "should fix indexes." do
      pregion = ParameterRegion.new([0.0, 0.0, 0.0], 0 => [2.0, 0.5], 1 => [1.0, 0.5], 2 => [0.5, 0.1])
      ary = pregion.fix_index(2, 1) do |ps|
        ps.index_varying.should == [0]
      end
    end

    it "should create parameters with fixing an index." do
      pregion = ParameterRegion.new([0.0, 0.0], 0 => [2.0, 0.5])
      ary = pregion.fix_index(0).map { |ps| ps.to_a.flatten }
      ary.should == [[0.0, 0.0], [0.5, 0.0], [1.0, 0.0], [1.5, 0.0], [2.0, 0.0]]
    end
  end

  def sum_for_region(*regions)
    sum = Array.new(regions[0].dim, 0.0)
    regions.each do |sp|
      sp.each do |val|
        sum.each_index do |i|
          sum[i] += val[i]
        end
      end
    end
    sum
  end

  def check_sum_for_region(sum1, sum2, err = 1e-8)
    sum1.each_index do |i|
      sum1[i].should be_within(sum2[i]).of(err)
    end
  end

  context "when the parmeter increases" do
    before(:all) do
      @pregion = ParameterRegion.new([0.0, 0.0, 0.0], 0 => [2.0, 0.5], 1 => [1.0, 0.03], 2 => [0.5, 0.1])
      @subregions = @pregion.split(1 => 5).to_a
      @ranges = @subregions.map do |ps|
        ps.range(1)
      end
    end

    it "should have 5 items." do
      @ranges.should have(5).items
    end

    it "should same end points as the original region." do
      @ranges[0][0].should == 0.0
      @ranges[-1][1].should == 1.0
    end

    it "should increasing end points." do
      (1...(@ranges.size)).each do |i|
        @ranges[i - 1][1].should < @ranges[i][1]
      end
    end

    it "should have same sum." do
      check_sum_for_region(sum_for_region(@pregion), sum_for_region(*@subregions))
    end
  end

  context "when the parameter decreases" do
    before(:all) do
      @pregion = ParameterRegion.new([0.0, 2.0, 0.0], 0 => [2.0, 0.5], 1 => [1.0, 0.03], 2 => [0.5, 0.1])
      @subregions = @pregion.split(1 => 5).to_a
      @ranges = @subregions.map do |ps|
        ps.range(1)
      end
    end

    it "should have 5 items." do
      @ranges.should have(5).items
    end

    it "should same end points as the original region." do
      @ranges[0][1].should == 2.0
      @ranges[-1][0].should == 1.0
    end

    it "should increasing end points." do
      (1...(@ranges.size)).each do |i|
        @ranges[i - 1][1].should > @ranges[i][1]
      end
    end

    it "should have same sum." do
      check_sum_for_region(sum_for_region(@pregion), sum_for_region(*@subregions))
    end
  end

  context "when twe indexes are splitted" do
    before(:all) do
      @pregion = ParameterRegion.new([0.0, 2.0, 0.0], 0 => [2.0, 0.5], 1 => [1.0, 0.3], 2 => [0.5, 0.2])
      @subregions = @pregion.split(1 => 2, 2 => 2).to_a.sort! do |sp1, sp2|
        n = sp1.range(1)[0] <=> sp2.range(1)[0]
        if n == 0
          sp1.range(2)[0] <=> sp2.range(2)[0]
        else
          n
        end
      end
    end

    it "should have 4 regions." do
      @subregions.should have(4).items
    end

    it "should split first dimension." do
      @subregions[0].range(1).should == @subregions[1].range(1)
      @subregions[2].range(1).should == @subregions[3].range(1)
      @subregions[0].range(1)[1].should < @subregions[2].range(1)[0]
    end

    it "should split second dimension." do
      @subregions[0].range(2).should == @subregions[2].range(2)
      @subregions[1].range(2).should == @subregions[3].range(2)
      @subregions[0].range(2)[1].should < @subregions[1].range(2)[0]
    end

    it "should have same sum." do
      check_sum_for_region(sum_for_region(@pregion), sum_for_region(*@subregions))
    end
  end

  context "when the splitting have end points" do
    before(:all) do
      @pregion = ParameterRegion.new([0.0, 0.0, 0.0], 0 => [1.0, 0.5], 1 => [1.0, 0.2], 2 => [0.6, 0.2])
      @subregions = @pregion.split(1 => 3).to_a
      @ranges = @subregions.map do |ps|
        ps.range(1)
      end
    end

    it "should have 3 items." do
      @ranges.should have(3).items
    end

    it "should same end points as the original region." do
      @ranges[0][0].should == 0.0
      @ranges[-1][1].should == 1.0
    end

    it "should increasing end points." do
      (1...(@ranges.size)).each do |i|
        @ranges[i - 1][1].should < @ranges[i][1]
      end
    end

    it "should have same sum." do
      check_sum_for_region(sum_for_region(@pregion), sum_for_region(*@subregions))
    end
  end
end

describe ParameterGrid do
  def check_point(pt, expected, err = 1e-8)
    pt.each_with_index do |v, i|
      v.should be_within(err).of(expected[i])
    end
  end

  def include_point(ary, included, err = 1e-8)
    ary.any? do |pt|
      ret = true
      pt.each_with_index do |val, i|
        if (val - included[i]).abs >= err
          ret = false
          break
        end
      end
      ret
    end.should be_true
  end

  context "when initializing" do
    it "should raise error for invalid dimension." do
      lambda do
        ParameterGrid.new([0.0, 0.0], [1.0, 2.0, 3.0])
      end.should raise_error
    end    
  end

  context "when getting property" do
    it "should get center point." do
      origin = [1.5, 3.5]
      pgrid = ParameterGrid.new(origin, [1.0, 2.0])
      pgrid.origin.should == origin
    end

    it "should get space sizes." do
      spaces = [1.0, 2.0]
      pgrid = ParameterGrid.new([1.5, 3.5], spaces)
      pgrid.space.should == spaces
    end

    it "should get space sizes (2)." do
      pgrid = ParameterGrid.new([1.5, 3.5, 0.0], 1.5)
      pgrid.space.should == [1.5, 1.5, 1.5]
    end
  end

  context "when getting points" do
    subject do
      ParameterGrid.new([1.2, -0.3, 0.5], [0.7, 0.5, 0.3])
    end

    it "should raise error for center arguments of invalid dimension." do
      lambda do
        subject.center(0, 0)
      end.should raise_error
    end

    it "should raise error for corner arguments of invalid dimension." do
      lambda do
        subject.corner(0, 0)
      end.should raise_error
    end

    it "should get a center point of [0, 0, 0]." do
      subject.center(0, 0, 0).should == subject.origin
    end

    it "should get a center point of [-2, 2, 2]." do
      check_point(subject.center(-2, 2, 2), [-0.2, 0.7, 1.1])
    end

    it "should get corner points of [0, 0, 0]." do
      pts = subject.corner(0, 0, 0)
      pts.should have(8).items
      include_point(pts, [0.85, -0.55, 0.35])
      include_point(pts, [0.85, -0.05, 0.35])
      include_point(pts, [0.85, -0.55, 0.65])
      include_point(pts, [0.85, -0.05, 0.65])
      include_point(pts, [1.55, -0.55, 0.35])
      include_point(pts, [1.55, -0.05, 0.35])
      include_point(pts, [1.55, -0.55, 0.65])
      include_point(pts, [1.55, -0.05, 0.65])
    end

    it "should get corner points of [-2, 2, 2]." do
      pts = subject.corner(-2, 2, 2)
      pts.should have(8).items
      include_point(pts, [-0.55, 0.45, 0.95])
      include_point(pts, [-0.55, 0.95, 0.95])
      include_point(pts, [-0.55, 0.45, 1.25])
      include_point(pts, [-0.55, 0.95, 1.25])
      include_point(pts, [0.15, 0.45, 0.95])
      include_point(pts, [0.15, 0.95, 0.95])
      include_point(pts, [0.15, 0.45, 1.25])
      include_point(pts, [0.15, 0.95, 1.25])
    end
  end

  context "when getting index" do
    subject do
      ParameterGrid.new([-0.7, -0.4, 0.3], [0.5, 0.5, 0.3])
    end

    it "should get index of origin." do
      subject.index([-0.7, -0.4, 0.3]).should == [0, 0, 0]
    end

    it "should get index of [0.2, 0.3, 0.0]." do
      subject.index([0.2, 0.3, 0.0]).should == [2, 1, -1]
    end

    it "should get index of [-0.3, -0.4, 0.9]." do
      subject.index([-0.3, -0.4, 0.9]).should == [1, 0, 2]
    end
  end

  context "when evaluate each center point" do
    subject do
      ParameterGrid.new([-0.7, -0.4, 0.3], [0.5, 0.5, 0.3])
    end

    it "should get region of [[0, 3], [-1, 1], [2, 4]]." do
      pts = subject.each_center([0, 3], [-1, 1], [2, 4]).to_a
      pts.should have(36).items
      pending
    end
  end
end
