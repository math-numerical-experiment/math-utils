require "spec_helper"

describe MkMfExt do
  it "should set LDFLAGS." do
    argv = ["--ldflags", "/home/user/dir"]
    described_class.set_ldflags(argv)
    argv.should be_empty
    $LDFLAGS.should match(/#{argv[1]}/)
  end

  it "should add -Wall option." do
    described_class.add_wall_option
    $CFLAGS.should match(/-Wall/)
  end
end
