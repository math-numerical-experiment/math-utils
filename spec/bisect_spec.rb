require "spec_helper"

def quadratic_func(x)
  x * x - 10.0
end

describe Bisect do
  context "when solving a quadratic equation" do
    it "should return a solution" do
      max_err = 0.00001
      bisect = Bisect.new
      bisect.set_objects(0.0, 10.0, :check => true) do |x|
        quadratic_func(x) < 0.0
      end
      bisect.set_creation do |x, y|
        (x + y) / 2.0
      end
      bisect.set_end_condition do |x, y|
        (x - y).abs < max_err
      end
      bisect.execute
      res = bisect.result
      res.should be_an_instance_of Array
      res.size.should == 2
      (res[0] - res[1]).abs.should < max_err
      (res[0] - Math.sqrt(10.0)).abs.should < max_err
    end

    it "should raise error for invalid initial values" do
      lambda do
        bisect = Bisect.new
        bisect.set_objects(0.0, 1.0, :check => true) do |x|
          quadratic_func(x) < 0.0
        end
      end.should raise_error(Bisect::InvalidInitialValue)
    end

    it "should raise error for invalid initial values" do
      lambda do
        bisect = Bisect.new
        bisect.set_objects(4.0, 8.0, :check => true) do |x|
          quadratic_func(x) < 0.0
        end
      end.should raise_error(Bisect::InvalidInitialValue)
    end

    it "should raise error for unfinished initialization" do
      lambda do
        bisect = Bisect.new
        bisect.set_objects(0.0, 8.0, :check => true) do |x|
          quadratic_func(x) < 0.0
        end
        bisect.execute
      end.should raise_error(Bisect::UnfinishedInitialization)
    end

    it "should not raise error" do
      results = true
      lambda do
        bisect = Bisect.new
        results = bisect.set_objects(0.0, 1.0, :check => true, :noerror => true) do |x|
          quadratic_func(x) < 0.0
        end
      end.should_not raise_error
      results[0].should be_false
      results[1].should be_true
      results[2].should be_true
    end
  end
end

describe Bisect::Number do
  context "when solving a quadratic equation" do
    it "should return a solution" do
      max_err = 0.00001
      bisect = Bisect::Number.new(max_err, 0.0, 10.0, :check => true) do |x|
        quadratic_func(x) < 0.0
      end
      bisect.execute
      res = bisect.result
      res.should be_an_instance_of Array
      res.size.should == 2
      (res[0] - res[1]).abs.should < max_err
      (res[0] - Math.sqrt(10.0)).abs.should < max_err
    end
  end
end

describe Bisect::ArrayIndex do
  context "when get a boundary index" do
    ary = [2, 4, 6, 7, 14, 21]
    bisect = Bisect::ArrayIndex.new(ary, :check => true) do |n|
      n % 2 == 0
    end
    bisect.execute
    res = bisect.result
    res.should == [2, 3]
  end
end
