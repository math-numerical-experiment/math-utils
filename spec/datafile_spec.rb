require "spec_helper"

describe DataFile do
  context "when loading hello.txt" do
    before(:all) do
      @lines = ["hello\n", "world\n"]
    end

    it "should yield on each line." do
      path = File.join(File.dirname(__FILE__), 'data/hello.txt')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end

    it "should yield on each line of xz file." do
      path = File.join(File.dirname(__FILE__), 'data/hello.txt.xz')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end

    it "should yield on each line of gzip file." do
      path = File.join(File.dirname(__FILE__), 'data/hello.txt.gz')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end
  end

  context "when loading number.txt" do
    before(:all) do
      @lines = ["12\n", "345\n", "6789\n", "0\n"]
    end

    it "should yield on each line." do
      path = File.join(File.dirname(__FILE__), 'data/number.txt')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end

    it "should yield on each line." do
      path = File.join(File.dirname(__FILE__), 'data/number.txt.xz')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end

    it "should yield on each line." do
      path = File.join(File.dirname(__FILE__), 'data/number.txt.gz')
      DataFile.each_line(path).with_index do |l, i|
        l.should == @lines[i]
      end
    end
  end
end
