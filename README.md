# math-utils

Miscellaneous files that math-utils creates for small usual programs.

## Bisect

    require 'misc/bisect'
    max_err = 0.00001
    bisect = Bisect.new
    bisect.set_objects(0.0, 10.0, :check => true) do |x|
      quadratic_func(x) < 0.0
    end
    bisect.set_creation do |x, y|
      (x + y) / 2.0
    end
    bisect.set_end_condition do |x, y|
      (x - y).abs < max_err
    end
    bisect.execute
    p bisect.result

    max_err = 0.00001
    bisect = Bisect::Number.new(max_err, 0.0, 10.0, :check => true) do |x|
      quadratic_func(x) < 0.0
    end
    bisect.execute
    p bisect.result


## Copyright

Copyright (c) 2011 Takayuki YAMAGUCHI. See LICENSE.txt for
further details.

