module MathUtils
  autoload :Bisect, "math_utils/bisect"
  autoload :DataFile, "math_utils/datafile"
  autoload :Grid, "math_utils/grid"
  autoload :LoggerByClass, "math_utils/logger_by_class"
  autoload :MkMfExt, "math_utils/mkmf_ext"
  autoload :ParameterRegion, "math_utils/parameter"
end
