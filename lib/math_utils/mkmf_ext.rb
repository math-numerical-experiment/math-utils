require "mkmf"
require "rubygems"

module MathUtils
  module MkMfExt
    def self.set_ldflags(argv = ARGV)
      i = 0
      while i < argv.size
        case argv[i]
        when '--ldflags'
          if args = argv[i+1]
            argv.delete_at(i)
            argv.delete_at(i)
            i -= 1
            $LDFLAGS += " #{args}"
          end
        end
        i += 1
      end
    end

    def self.add_wall_option
      $CFLAGS += " -Wall"
    end

    class LoadHeaders
      attr_reader :headers

      # @param [Hash] headers Keys are gem names and values are header files.
      def initialize(headers)
        @headers = headers
      end

      def find
        @headers.each do |gem_name, header_names|
          if spec = Gem::Specification.find_by_name(gem_name)
            header_names.each do |header_name|
              if Array === header_name
                header_file = header_name[0]
                directory = File.join(spec.full_gem_path, *header_name[1..-1])
              else
                header_file = header_name
                directory = spec.full_gem_path
              end
              dirs = Dir.glob("#{directory}/**/*").select { |path| File.directory?(path) }
              found = false
              dirs.each do |d|
                if find_header(header_file, d)
                  found = true
                  break
                end
              end
              unless found
                raise "#{header_file} is not found."
              end
            end
          else
            raise "Can not find gem '#{gem_name}'."
          end
        end
      end
    end
  end
end
