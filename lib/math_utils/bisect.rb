module MathUtils
  # Class for bisection method.
  class Bisect
    class InvalidInitialValue < StandardError
    end

    class UnfinishedInitialization < StandardError
    end

    # Set the two objects for bisection methods.
    # +block.call(obj_true)+ must return true and
    # +block.call(obj_false)+ must returns false.
    # If +opts[:check]+ is ture, this method raise error
    # for invalid +obj_true+ or invalid +obj_false+.
    # If we do not raise error for checking objects,
    # we set the option :noerror true.
    def set_objects(obj_true, obj_false, opts = {}, &block)
      @obj_true = obj_true
      @obj_false = obj_false
      @judge = block
      check_initial_objects(opts[:noerror]) if opts[:check]
    end

    def check_object(obj)
      @judge.call(obj)
    end
    private :check_object

    def check_initial_objects(noerror)
      results = [check_object(@obj_true), check_object(@obj_false)]
      if noerror
        if results[0] && !results[1]
          [true] + results
        else
          [false] + results
        end
      else
        unless results[0]
          raise InvalidInitialValue, "#{@obj_true.inspect.strip} does not return true."
        end
        if results[1]
          raise InvalidInitialValue, "#{@obj_false.inspect.strip} does not return false."
        end
      end
    end
    private :check_initial_objects

    # +block+ accepts two arguments and returns an in-between variable of the two arguments.
    # The arguments and the return value must be in same class and
    # be aruments of the method set by +set_objects+ method.
    def set_creation(&block)
      @create = block
    end

    # Set the end condition of bisection method. +block+ accepts two arguments.
    # If +block.call(obj_true, obj_false)+ is true, the bisection method stops.
    def set_end_condition(&block)
      @end_condition = block
    end

    def check_variables
      unless (@obj_true && @obj_false && @judge && @create && @end_condition)
        raise UnfinishedInitialization, "Initialization of bisection method has not finished yet."
      end
    end
    private :check_variables

    # Start the bisection method.
    # Returned value is the same as that of method +resulut+.
    def execute
      check_variables
      begin
        obj_new = @create.call(@obj_true, @obj_false)
        if check_object(obj_new)
          @obj_true = obj_new
        else
          @obj_false = obj_new
        end
      end while !@end_condition.call(@obj_true, @obj_false)
      result
    end

    # Return result of the bisection method:
    # an array of two objects.
    # First element is an object of true and
    # second element is an object of false.
    def result
      [@obj_true, @obj_false]
    end

    # Class for bisection method for floating point numbers like Newton's method.
    class Number < Bisect

      # We repeats calculations for medium number of two numbers.
      # +block+ take a number as an argrument.
      # +block.call(num_true)+ must return true and +block.call(num_false)+ must return false.
      # If (+num_true+ - +num_false+).abs is less than +max_err+,
      # the bisection method stops.
      def initialize(max_err, num_true, num_false, opts = {}, &block)
        set_objects(num_true, num_false, opts, &block)
        set_creation do |x, y|
          (x + y) / 2.0
        end
        set_end_condition do |x, y|
          (x - y).abs < max_err
        end
      end
    end

    # Bisection method for indices of an array (or an object that have methods [] and size).
    class ArrayIndex < Bisect
      
      # For a proc object the elements from first to some index return true and
      # the elements from some index to last return false.
      # That is to say, we must sort the index in advance.
      # Then, We can get the boundary index.
      # +ary+ is a target object and +opts+ is the same as +opts+ of Bisect#set_objects.
      # +block+ take an element of +ary+ as an argrument.
      # +block.call(ary[0])+ must return true and +block.call(ary[ary.size - 1])+ must return false.
      # If absolute value of subtraction of two indices equals one,
      # the bisection method stops.
      def initialize(ary, opts = {}, &block)
        set_objects(0, ary.size - 1, opts) do |i|
          block.call(ary[i])
        end
        set_creation do |x, y|
          n = (x + y) / 2
          if n == x
            n + 1
          else
            n
          end
        end
        set_end_condition do |x, y|
          (x - y).abs == 1
        end
      end
    end
  end
end
