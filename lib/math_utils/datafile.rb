require 'zlib'
require 'xz'

module MathUtils
  module DataFile
    class XZFile
      def initialize(path, rs = $/)
        @path = path
        @rs = rs
      end

      def get_line(s)
        if n = s.index(@rs)
          s.slice!(0..n)
        else
          nil
        end
      end
      private :get_line

      def each_line(&block)
        if block_given?
          Kernel.open(@path, 'rb') do |io|
            buf = ''
            XZ.decompress_stream(io) do |chunk|
              buf << chunk
              while t = get_line(buf)
                yield(t)
              end
            end
            while t = get_line(buf)
              yield(t)
            end
            yield(buf) if buf.size > 0
          end
        else
          to_enum(:each_line)
        end
      end
    end

    def self.xz_each_line(filename, &block)
      DataFile::XZFile.new(filename).each_line(&block)
    end

    def self.each_line(filename, &block)
      if block_given?
        case filename
        when /\.xz$/
          self.xz_each_line(filename, &block)
        when /\.gz$/
          Zlib::GzipReader.open(filename) do |gz|
            while l = gz.gets
              yield(l)
            end
          end
        else
          Kernel.open(filename, 'r') do |f|
            while l = f.gets
              yield(l)
            end
          end
        end
      else
        to_enum(:each_line, filename)
      end
    end
  end
end
