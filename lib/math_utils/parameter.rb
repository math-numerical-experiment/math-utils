module MathUtils
  class ParameterRegion
    include Enumerable
    
    # @param [Array] prms Starting point of parameter range
    # @param [Hash] range End point of parameter range
    #  A key means an index and a value is an array of a max value and a step size.
    # @example
    #  prm_region = ParameterRegion.new([0.0, 0.0], 0 => [1.0, 0.5], 1=> [1.0, 0.3])
    def initialize(prms, range = {})
      # It is possible that range is an array of pairs of index and parameter range.
      @prms = prms
      range.each do |k, v|
        raise ArgumentError, "Invalid index: #{k.inspect}" unless Fixnum === k
        raise ArgumentError, "Invalid value of #{k}: #{v.inspect}" unless Array === v
      end
      @range = range.to_a.sort_by { |a| a[0] }
      raise ArgumentError, "Invalid range: out of parameter dimension." if @range.size > 0 && !@prms[@range[-1][0]]
    end
    
    def parameter_vary?(index)
      @range.assoc(index)
    end
    private :parameter_vary?
    
    def nth_index(index)
      @range.find_index { |a| a[0] == index }
    end
    private :nth_index
    
    def dim
      @prms.size
    end
    
    def index_varying
      @range.map do |a|
        a[0]
      end.sort
    end
    
    def range(index)
      if prm_range = parameter_vary?(index)
        [@prms[index], prm_range[1][0]].sort
      else
        [@prms[index], @prms[index]]
      end
    end
    
    def range_property(nth)
      if v = @range[nth]
        [v[0], v[1][0], v[1][1].abs]
      else
        nil
      end
    end
    private :range_property
    
    def each_parameter(prms_src, nth, &block)
      prms = prms_src.dup
      index, prm_end, step = range_property(nth)
      if prms[index] <= prm_end
        while prms[index] <= prm_end
          yield(prms.dup)
          prms[index] += step
        end
      else
        while prms[index] >= prm_end
          yield(prms.dup)
          prms[index] -= step
        end
      end
    end
    private :each_parameter
    
    def recursive_block(prms_src, nth, s, &block)
      if s < 0
        yield(prms_src.dup)
      elsif nth == s
        each_parameter(prms_src, nth) do |prms|
          yield(prms)
        end
      else
        each_parameter(prms_src, nth) do |prms|
          recursive_block(prms, nth + 1, s, &block)
        end
      end
    end
    private :recursive_block
    
    def each(&block)
      if block_given?
        recursive_block(@prms, 0, @range.size - 1, &block)
      else
        to_enum(:each)
      end
    end
    
    def duplicate_range_completely
      Marshal.load(Marshal.dump(@range))
    end
    private :duplicate_range_completely
    
    def fix_index(index, *indexes, &block)
      if block_given?
        if nth = nth_index(index)
          range_src = duplicate_range_completely.delete_if { |a| a[0] == index }
          if indexes.size == 0
            each_parameter(@prms, nth) do |prm|
              yield(self.class.new(prm, range_src))
            end
          else
            each_parameter(@prms, nth) do |prm|
              self.class.new(prm, range_src).fix_index(*indexes, &block)
            end
          end
        else
          yield(self)
        end
      else
        to_enum(:fix_index, index)      
      end
    end
    
    def split_range(range, num_split)
      index = range[0]
      step = range[1][1].abs
      prm = @prms[index]
      prm_end = range[1][0]
      total = (((prm - prm_end) / step).abs.floor + 1.0).to_f
      avg = total / num_split
      splitted_range = []
      num_rest = num_split
      if prm <= prm_end
        while total > 0
          splitted_range << [prm, [prm + step * (avg.ceil - 0.5), prm_end].min]
          prm += (step * avg.ceil)
          total -= avg.ceil
          num_split -= 1
          avg = total / num_split
        end
      else
        while total > 0
          splitted_range << [prm, [prm - step * (avg.ceil - 0.5), prm_end].max]
          prm -= (step * avg.ceil)
          total -= avg.ceil
          num_split -= 1
          avg = total / num_split
        end
      end
      splitted_range
    end
    private :split_range
    
    def each_splitted_region(index, splitted_range, &block)
      splitted_range.each do |prm_start, prm_end|
        prms = @prms.dup
        range_src = duplicate_range_completely
        prms[index] = prm_start
        range_src.each do |a|
          if a[0] == index
            a[1][0] = prm_end
          end
        end
        yield(self.class.new(prms, range_src))
      end
    end
    private :each_splitted_region
    
    # @param [Hash] cond The condition of splitting. A key is index and a value is number of splitting.
    def split(cond, &block)
      if block_given?
        cond_ary = cond.to_a
        raise ArgumentError, "No condition to split parameter region." if cond_ary.empty?
        index, num_split = cond_ary.shift
        raise ArgumentError, "Can not split for index #{index}." unless range = parameter_vary?(index)
        splitted_range = split_range(range, num_split)
        if cond_ary.empty?
          each_splitted_region(index, splitted_range, &block)
        else
          each_splitted_region(index, splitted_range) do |sp|
            sp.split(cond_ary.dup, &block)
          end
        end
      else
        to_enum(:split, cond)
      end
    end
  end
  
  class ParameterGrid
    attr_reader :origin, :space, :dim
    
    # @param [Array] origin Origin point of grid
    # @param [Float, Array] space Space size of grid
    def initialize(origin, space)
      @origin = origin
      @dim = @origin.size
      if Array === space
        @space = space
        check_dimension(@space, "Invalid dimension of origin point and space size.")
      else
        @space = Array.new(@dim, space)
      end
      @corner = @dim.times.map do |i|
        @origin[i] - @space[i] / 2.0
      end
    end
    
    def check_dimension(args, mes = nil)
      if @dim != args.size
        raise ArgumentError, mes || "Invalid dimension"
      end
    end
    private :check_dimension
    
    def point_of_index(pt, indexes)
      @dim.times.map do |i|
        pt[i] + @space[i] * indexes[i]
      end
    end
    private :point_of_index
    
    def center_point(indexes)
      point_of_index(@origin, indexes)
    end
    
    def corner_point(indexes)
      point_of_index(@corner, indexes)
    end
    private :corner_point
    
    def center(*indexes)
      check_dimension(indexes)
      center_point(indexes)
    end
    
    # Return corner points.
    def corner(*indexes)
      check_dimension(indexes)
      pt_min_max = [corner_point(indexes), corner_point(indexes.map { |i| i + 1 })]
      (2 ** @dim).times.map do |num|
        @dim.times.map do |i|
          num, r = num.divmod(2)
          pt_min_max[r][i]
        end
      end
    end
    
    def index(pt)
      check_dimension(pt)
      @dim.times.map do |i|
        ((pt[i] - @corner[i]) / @space[i]).floor
      end
    end
    
    # @param [Array] An array of pair of integers.
    def each_center(*index_args, &block)
      check_dimension(index_args)
      cen = center_point(index_args.map { |ary| ary.min })
      cor = corner_point(index_args.map { |ary| ary.max + 1 })
      ParameterRegion.new(cen, @dim.times.map { |i| [i, [cor[i], @space[i]]] }).each(&block)
    end
  end
end
