require 'logger'
require 'singleton'

module MathUtils
  class LoggerByClass
    # See ManageLog::Manager.set_default_conf.
    def self.set_default_conf(hash)
      ManageLog::Manager.instance.set_default_conf(hash)
    end
    
    # See ManageLog::Manager.set_log_conf.
    def self.set_log_conf(class_name, hash)
      ManageLog::Manager.instance.set_log_conf(class_name, hash)
    end
    
    # See ManageLog::Manager.get_new_logger.
    def self.get_new_logger(class_name = :default)
      ManageLog::Manager.instance.get_new_logger(class_name)
    end
    
    # See ManageLog::Manager.get_logger.
    def self.get_logger(transfered_logger, class_name = :default)
      ManageLog::Manager.instance.get_logger(transfered_logger, class_name)
    end
    
    def self.help_message
      "Level of logging are 'FATAL', 'ERROR', 'WARN', 'INFO' or 'DEBUG'."
    end
    
    def self.log_level_from_arg(arg_str)
      if /^f/i =~ arg_str
        Logger::FATAL
      elsif /^e/i =~ arg_str
        Logger::ERROR
      elsif /^w/i =~ arg_str
        Logger::WARN
      elsif /^i/i =~ arg_str
        Logger::INFO
      elsif /^d/i =~ arg_str
        Logger::DEBUG
      else
        raise ArgumentError, "Invalid level of logging."
      end
    end
  end
  
  
  
  
  
  module LogMessage
    def self.program_name
      $PROGRAM_NAME + (ARGV.size > 0 ? ' ' + ARGV.join(" ") : "")
    end
    
    def self.time_now
      Time.now.strftime("%c")
    end
    
  end
  
  # 
  # Create logger for log level of Class.
  #
  # Usage
  # class Test
  #   def initialize
  #     @logger = ManageLog.get_new_logger(self.class)
  #   end
  # end
  # ManageLog.set_log_conf(Test, :level => Logger::WARN, :io => STDOUT)
  #
  module ManageLog
    
    # The instance of this class has data for logging.
    # This class include Singleton, so there is unique instance.
    class Manager
      include Singleton
      
      def initialize
        @data = { }
        @data[:default] = { :level => Logger::INFO, :io => STDOUT }
      end
      
      # Check whether hash is valid for configuration.
      def check(hash)
        if hash.has_key?(:io) && hash.has_key?(:level) &&
            (hash[:level] == Logger::DEBUG || hash[:level] == Logger::INFO || hash[:level] == Logger::WARN ||
             hash[:level] == Logger::ERROR || hash[:level] == Logger::FATAL)
          hash
        else
          raise ArgumentError, "Invalid argument hash."
        end
      end
      private :check
      
      # Set default configuration. See set_log_conf for detail of hash.
      def set_default_conf(hash)
        @data[:default] = check(hash)
      end
      
      # Set configuration for class_name by hash.
      # The example of hash is { :level => Logger::INFO, :io => STDOUT }.
      # hash[:level] is Logger::DEBUG, Logger::INFO, Logger::WARN, Logger::ERROR or Logger::FATAL.
      def set_log_conf(class_name, hash)
        @data[class_name] = check(hash)
      end
      
      # Initialize logger for class_name.
      def init_logger(class_name)
        logger = Logger.new(@data[class_name][:io])
        logger.level = @data[class_name][:level]
        logger
      end
      private :init_logger
      
      # Return logger for class_name.
      def get_new_logger(class_name = :default)
        init_logger((@data.has_key?(class_name) ? class_name : :default))
      end
      
      # If transfered_logger is not Logger, return new logger.
      def get_logger(transfered_logger, class_name = :default)
        Logger === transfered_logger ? transfered_logger : get_new_logger(class_name)
      end
    end
  end
end
